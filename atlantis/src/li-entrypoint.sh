#!/bin/bash

set -e

if [[ -z ${ATLANTIS_DATA_DIR} ]]; then
  ATLANTIS_DATA_DIR="/home/atlantis"
fi

# Modified: https://github.com/hashicorp/docker-consul/blob/2c2873f9d619220d1eef0bc46ec78443f55a10b5/0.X/docker-entrypoint.sh

# If the user is trying to run atlantis directly with some arguments, then
# pass them to atlantis.
if [ "${1:0:1}" = '-' ]; then
  set -- atlantis "$@"
fi

# If the user is running an atlantis subcommand (ex. server) then we want to prepend
# atlantis as the first arg to exec. To detect if they're running a subcommand
# we take the potential subcommand and run it through atlantis help {subcommand}.
# If the output contains "atlantis subcommand" then we know it's a subcommand
# since the help output contains that string. For anything else (ex. sh)
# it won't contain that string.
# NOTE: We use grep instead of the exit code since help always returns 0.
if atlantis help "$1" 2>&1 | grep -q "atlantis $1"; then
  # We can't use the return code to check for the existence of a subcommand, so
  # we have to use grep to look for a pattern in the help output.
  set -- atlantis "$@"
fi

# If the current uid running does not have a user create one in /etc/passwd
if ! whoami &>/dev/null; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:/home/atlantis:/sbin/nologin" >>/etc/passwd
  fi
fi

# Generate .terraformrc file
cat <<-EOF >"${ATLANTIS_DATA_DIR}/.terraformrc"
plugin_cache_dir = "${ATLANTIS_DATA_DIR}/.terraform.d/plugin-cache"
EOF
mkdir -p "${ATLANTIS_DATA_DIR}/.terraform.d/plugin-cache"

# Generate .gitconfig file
cat <<-EOF >"${ATLANTIS_DATA_DIR}/.gitconfig"
[url "https://${GITHUB_TOKEN}@github.com"]
insteadOf = ssh://git@github.com
EOF

exec "$@"
