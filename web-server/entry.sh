#!/usr/bin/env bash

cp /etc/nginx/conf.d/default.tpl /etc/nginx/conf.d/default.conf

sed -e "s/__PATH__/${CI_PROJECT_PATH}/" \
/etc/nginx/conf.d/default.conf

/etc/init.d/nginx restart

exec "$@"
