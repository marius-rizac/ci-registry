<?php

require_once __DIR__ . '/vendor/autoload.php';

use Kisphp\Kisdb;
use Kisphp\KisdbLogger;


function check_db_connection($host, $user, $pass, $dbName='kisphp')
{
    $dbLog = new KisdbLogger();
    $db = new Kisdb($dbLog);

    // optional, enable logs
    $dbLog->enableDebug();

    $db->connect(
        $host,      // localhost
        $user,  // root
        $pass,  // {brank}
        $dbName,      // test,
//        'mysql',            // database driver
//        [                   //
//            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
//        ]
    );

    // get last query
    $logs = $db->getLogger()->getLogs();
    $error = $db->getStatus();

    printf("User: %s, Pass: %s, Connected: %s\n", $user, $pass, implode(', ', $error));
}

check_db_connection('127.0.0.1', 'root', 'debian');
check_db_connection('127.0.0.1', 'marius', 'mariusplain');
check_db_connection('127.0.0.1', 'bogdan', 'bogdansha2');
check_db_connection('127.0.0.1', 'lavinia', 'lavinia');
