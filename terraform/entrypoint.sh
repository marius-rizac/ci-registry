#!/bin/bash

if [[ -z $TERRAFORM_VERSION ]]; then
  TERRAFORM_VERSION="1.9.3"
fi

cat <<EOF > ~/.aws/config
    [default]
    region = us-east-1
    output = json
EOF

exec "$@"
