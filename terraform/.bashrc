export HISTCONTROL=ignoreboth:erasedups

# export PATH="${PATH}:/root/.local/bin"

tfnew () {
  VERSION="${1}"

  if [[ -z "${VERSION}" ]];then
    echo "Please provide which version you want o install"
    return 1
  fi

  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
      KERNEL_NAME="linux"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
      KERNEL_NAME="darwin"
  else
      echo "-- Platform not supported: ${OSTYPE}"
      return 1
  fi

  ARCH=$(uname -m)
  if [[ $ARCH == x86_64* ]]; then
    MACHINE="amd64"
  elif [[ $ARCH == arm* ]]; then
    MACHINE="arm64"
  else
    echo "-- Architecture not supported: ${ARCH}"
    return 1
  fi

  echo "Terraform version: ${VERSION}"

  if [[ ! -f "/opt/terraform/${VERSION}/terraform" ]];then
    echo "Downloading Terraform: ${VERSION}"
    wget https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
    && unzip terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
    && rm terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
    && mkdir -p /opt/terraform/${VERSION} \
    && mv terraform /opt/terraform/${VERSION}/
  fi

  echo "Setting terraform ${VERSION} as global"
  ln -sf /opt/terraform/${VERSION}/terraform /usr/local/bin/terraform
  return 0
}

tfnew "${TERRAFORM_VERSION}"

# echo "-------------------------------"
# echo "terraform version $(terraform version -json | jq -r '.terraform_version')"
# terragrunt -version
# infracost --version
# pre-commit --version
# tflint --version
# echo "-------------------------------"

kpxup () {
#     cat <<EOF > ~/.aws/credentials
#     [default]
#     AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
#     AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"
#     AWS_SESSION_TOKEN="${AWS_SESSION_TOKEN}"
# EOF

    cat <<EOF > ~/.aws/config
    [default]
    region = us-east-1
    output = json
EOF
}
