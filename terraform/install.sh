#!/bin/bash

set -ex

TARGET="/opt"

TFDOCS_VERSION="v0.19.0"
#TERRAGRUNT_VERSION="v0.55.18"
INFRACOST_VERSION="v0.10.39"
TFLINT_VERSION="v0.50.0"

# Terraform
tfnew () {
  VERSION="${1}"

  if [[ -z "${VERSION}" ]];then
    echo "Please provide which version you want o install"
    return 1
  fi

  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
      KERNEL_NAME="linux"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
      KERNEL_NAME="darwin"
  else
      echo "-- Platform not supported: ${OSTYPE}"
      return 1
  fi

  ARCH=$(uname -m)
  if [[ $ARCH == x86_64* ]]; then
    MACHINE="amd64"
  elif [[ $ARCH == arm* ]]; then
    MACHINE="arm64"
  elif [[ $ARCH == aarch64 ]]; then
    MACHINE="arm64"
  else
    echo "-- Architecture not supported: ${ARCH}"
    return 1
  fi
#  KERNEL_NAME="linux"
#  MACHINE="amd64"

  echo "Downloading Terraform: ${VERSION}"
  wget -q https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
  && unzip terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
  && rm terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
  && mkdir -p ${TARGET}/tf/${VERSION} \
  && mv ./terraform ${TARGET}/tf/${VERSION}/
}

for VERSION in ${AVAILABLE_TERRAFORM_VERSIONS};
do
  tfnew "${VERSION}"
done

mkdir -p "${TARGET}/dist"

## Terragrunt
#echo "Installing Terragrunt"
#wget -q https://github.com/gruntwork-io/terragrunt/releases/download/${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 \
#&& mv terragrunt_linux_amd64 ${TARGET}/dist/terragrunt

# Terraform docs
echo "Installing Terraform docs"
wget -q https://github.com/terraform-docs/terraform-docs/releases/download/${TFDOCS_VERSION}/terraform-docs-${TFDOCS_VERSION}-linux-amd64.tar.gz \
&& tar -xzvf terraform-docs-${TFDOCS_VERSION}-linux-amd64.tar.gz \
&& mv terraform-docs ${TARGET}/dist/terraform-docs

# Infracost
echo "Installing Infracost"
curl -s -L https://github.com/infracost/infracost/releases/download/$INFRACOST_VERSION/infracost-linux-amd64.tar.gz | tar xz -C /tmp \
&& mv /tmp/infracost-linux-amd64 ${TARGET}/dist/infracost

# TFLINT
echo "Installing TFLINT"
wget -q https://github.com/terraform-linters/tflint/releases/download/${TFLINT_VERSION}/tflint_linux_amd64.zip \
&& unzip -d /tmp tflint_linux_amd64.zip \
&& mv /tmp/tflint ${TARGET}/dist/tflint

# Make all additional tools executable
chmod +x ${TARGET}/dist/*
