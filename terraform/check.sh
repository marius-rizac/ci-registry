#!/bin/bash

CURL_OPTIONS=""
# used for debugging
#CURL_OPTIONS="-H 'Authorization: token ${GITHUB_TOKEN}'"
echo ""

#TERRAGRUNT_VERSION_INSTALLED=$(terragrunt --version | awk '{print $3}')
#TERRAGRUNT_VERSION=$(curl https://api.github.com/repos/gruntwork-io/terragrunt/releases/latest -s | jq .tag_name -r)
#echo "Terragrunt: ${TERRAGRUNT_VERSION} (Installed: ${TERRAGRUNT_VERSION_INSTALLED})"

TERRAFORM_VERSION_INSTALLED=$(terraform -version | grep Terraform | head -1 | awk '{print $2}')
TERRAFORM_VERSION=$(curl "$CURL_OPTIONS" https://api.github.com/repos/hashicorp/terraform/releases/latest -s | jq .tag_name -r)
echo "Terraform: ${TERRAFORM_VERSION} (Installed: ${TERRAFORM_VERSION_INSTALLED})"

TFDOCS_VERSION_INSTALLED=$(terraform-docs -v | awk '{print $3}')
TFDOCS_VERSION=$(curl "$CURL_OPTIONS" https://api.github.com/repos/terraform-docs/terraform-docs/releases/latest -s | jq .tag_name -r)
echo "Terraform Docs: ${TFDOCS_VERSION} (Installed: ${TFDOCS_VERSION_INSTALLED})"

INFRACOST_VERSION_INSTALLED=$(infracost -v 2>/dev/null | grep 'Infracost v' | awk '{print $2}')
INFRACOST_VERSION=$(curl "$CURL_OPTIONS" https://api.github.com/repos/infracost/infracost/releases/latest -s | jq .tag_name -r)
echo "Infracost: ${INFRACOST_VERSION} (Installed: ${INFRACOST_VERSION_INSTALLED})"

TFLINT_VERSION_INSTALLED=$(tflint --version | grep 'TFLint version' | awk '{print $3}')
TFLINT_VERSION=$(curl "$CURL_OPTIONS" https://api.github.com/repos/terraform-linters/tflint/releases/latest -s | jq .tag_name -r)
echo "TFLINT: ${TFLINT_VERSION} (Installed: ${TFLINT_VERSION_INSTALLED})"

echo "AWS cli version:"
aws --version

echo "Kubectl version: $(kubectl version --client)"
echo "helm version: $(helm version --short)"
