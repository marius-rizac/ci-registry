#!/usr/bin/env python3

# curl -sH "Accept: application/vnd.github.v3+json" https://api.github.com/repos/hashicorp/terraform/releases > /tmp/terraform-versions.json

import json

with open('/tmp/terraform-versions.json', 'r') as fh:
    content = json.loads(fh.read())

    releases = []
    for version in content:

        if version["draft"] is True or version["prerelease"] is True:
            continue

        releases.append(version['tag_name'].replace('v', ''))

    with open('/tmp/terraform-versions.txt', 'w') as fw:
        fw.write("\n".join(releases))
