List a directory content as a web app to download files

```shell
docker run --rm \
-v /home/username/Music:/app \
-p 8080:80 \
-it kisphp-apache-index
```

If you use dotfiles run:

```shell
# make sure the first parameter is always the absolute path to a directory
dcshow $(pwd)
```
